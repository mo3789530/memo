###  mysql 

- pull
`dokcer pull mysql`  

- run docker
`docker run --name mysql -e MYSQL_ROOT_PASSWORD=password -d -p 3306:3306 mysql`

- mysql
`mysql -h hostname -uroot -p`  

- exec
`docker exec -it dockername or id  bash`  

