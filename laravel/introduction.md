### 概要  

アプリケーションに対する最初のリクエストは`public/index.php'

### ルーティング   
web: `routes/web.php`   
Api: `routes/api.php`  

routeの確認方法`php artisan route:list`

* redirect  
`Route::redirect('/here', '/thre');`
* view  
`Route::view('/welcome', 'welcome');  
* parmater
`Route::get('user/{id}', function($id));`
* parmater(複数)
`Route::get('posts/{post}/comments/{comment}', function ($postId, $commentId) {
    //
});`

任意パラメータの場合はパラメータ名の後に?を付ける

`Route::get('user/{name?}', function ($name = null) {
    return $name;
});`   

## Route Group  
* middlewareの指定
```
Route::middleware(['first', 'second'])->group(function () {
    Route::get('/', function () {
        // firstとsecondミドルウェアを使用
    });

    Route::get('user/profile', function () {
        // firstとsecondミドルウェアを使用
    });
});
```

* 名前空間の指定
```
Route::middleware(['first', 'second'])->group(function () {
    Route::get('/', function () {
        // firstとsecondミドルウェアを使用
    });

    Route::get('user/profile', function () {
        // firstとsecondミドルウェアを使用
    });
});
```

* rote prefix
```
Route::prefix('admin')->group(function () {
    Route::get('users', function () {
        // Matches The "/admin/users" URL
    });
});
```

### midlleware
midellewareはアプリケーションへ送信されるHTTPリクエストをフィルタリングする
Httpリクエストがアプリケーションに届くまでに通過するレイヤーと考えるといい
* midllewareの作成
`php artisan make:middleware CheckAge` 

* midllewareをRuteへの登録  
特定のルートのみに対してmidellewareを指定する場合は、`app/Http/kernel.php`に登録する必要がある  

### Controller
* RouteからControllerへ紐づけ  
`Route::get('user/{id}', 'コントローラー名@メソッド名');`  

* CRUDを作成  
`php artisan make:controller PhotoController --resource`
Routeへの追加  
`Route::resource('photos', 'PhotoController');`  
apiでcreateとeditを省く場合
`Route::apiResource('photos', 'PhotoController');`


