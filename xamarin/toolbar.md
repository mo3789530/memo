toolbarを使うためにはNavigationPageの必要がある

App.xaml.cs
```
            MainPage = new NavigationPage(new MainPage());
```

MainPage
```
			ToolbarItems.Add(new ToolbarItem
			{
				Text = "新規作成",
				Priority = 1,
				Order = ToolbarItemOrder.Primary,
				Command = new Command(() =>
				{
					//ボタンを押下した際の処理を記述します。
				}),
			});    
```